<?php include 'views/templates/head.php'; ?>
<?php include 'views/sections/navbar.php'; ?>
<div class="content">
    <div class="buynow-orderdevice">
        <div class="container pb-5">
            <div class="row  justify-content-center">
                <h3 class="heading  text-white  font-weight-normal  mb-5">GET YOUR AUDRA DEVICES TODAY</h3>
            </div>
            <div class="row grid-divider">
                <div class="col-md-6 my-1">
                    <div class="row">
                        <img src="assets/images/product-BizSecure.png" alt="BizSecure" class="d-block  mx-auto  mb-2" width="auto" height="200"/>
                        <div class="col-6 buynow-product">
                            <h3 class="text-white">BizSecure</h3>
                            <span class="half-underline"></span>
                            <b>RM60</b> monthly<br/>
                            RM299 yearly signup<br/>

                            <div class="d-flex  justify-content-center">

                                <div class="input-group">
                                    <label for="input-group" class="align-middle">Quantity:</label>
                                    <input type="button" value="-" class="button-minus navy" data-field="quantity">
                                    <input type="number" step="1" max="" value="1" name="quantity" class="quantity-field">
                                    <input type="button" value="+" class="button-plus navy" data-field="quantity">
                                </div>
                            </div>

                            <div class="banners-action mb-2">
                                <a href="#" class="btn btn-pill btn-secondary navy">
                                    <span>Buy Now</span>
                                    <svg class="icon-chevron-next" width="24" height="24" fill="currentColor"><use xlink:href="#icon-chevron-next"></use></svg>
                                </a>
                            </div>
                            <u><a href="#" class="">View technical specifications  ></a></u>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 my-1">
                    <div class="row">
                        <img src="assets/images/product-HomeShield.png" alt="HomeShield" class="d-block  mx-auto  mb-2" width="auto" height="220"/>
                        <div class="col-6 buynow-product">
                            <h3 class="text-white">HomeShield</h3>
                            <span class="half-underline"></span>
                            <b>RM35</b> monthly<br/>
                            RM299 yearly signup<br/>

                            <div class="d-flex  justify-content-center">

                                <div class="input-group">
                                    <label for="input-group" class="align-middle">Quantity:</label>
                                    <input type="button" value="-" class="button-minus brilliant-orange" data-field="quantity">
                                    <input type="number" step="1" max="" value="1" name="quantity" class="quantity-field">
                                    <input type="button" value="+" class="button-plus brilliant-orange" data-field="quantity">
                                </div>
                            </div>

                            <div class="banners-action mb-2">
                                <a href="#" class="btn btn-pill btn-secondary brilliant-orange">
                                    <span>Buy Now</span>
                                    <svg class="icon-chevron-next" width="24" height="24" fill="currentColor"><use xlink:href="#icon-chevron-next"></use></svg>
                                </a>
                            </div>
                            <u><a href="#" class="">View technical specifications  ></a></u>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="buynow-feature">
        <div class="container">
            <div class="row">
                <div class="col-md-5 justify-content-center align-self-center">
                    <h1 class="h2  heading  text-uppercase  font-weight-normal">why look further? <br/>We’ve got you covered.</h1>
                    <div class="mb-5">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="">
                        <table class="table table-sm table-bordered table-feature">
                            <thead>
                                <tr>
                                    <th scope="col">Key features</th>
                                    <th scope="col"><b>AUDRA</b></th>
                                    <th scope="col">Standard Router</th>
                                    <th scope="col">Antivirus</th>
                                    <th scope="col">Firewall</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">Easy Setup</th>
                                    <td><div class="icon-checkmark"></div></td>
                                    <td>Some</td>
                                    <td><div class="icon-checkmark"></div></td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <th scope="row">Simple UI</th>
                                    <td><div class="icon-checkmark"></div></td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <th scope="row">No Maintenance</th>
                                    <td><div class="icon-checkmark"></div></td>
                                    <td>Some</td>
                                    <td>-</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <th scope="row">Parental Controls</th>
                                    <td><div class="icon-checkmark"></div></td>
                                    <td>-</td>
                                    <td>Some</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <th scope="row">Employee Productivity</th>
                                    <td><div class="icon-checkmark"></div></td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <th scope="row">Stop Data Leaks</th>
                                    <td><div class="icon-checkmark"></div></td>
                                    <td>-</td>
                                    <td><div class="icon-checkmark"></div></td>
                                    <td><div class="icon-checkmark"></div></td>
                                </tr>
                                <tr>
                                    <th scope="row">Stop Adware</th>
                                    <td><div class="icon-checkmark"></div></td>
                                    <td>-</td>
                                    <td><div class="icon-checkmark"></div></td>
                                    <td><div class="icon-checkmark"></div></td>
                                </tr>
                                <tr>
                                    <th scope="row">Instant Alert</th>
                                    <td><div class="icon-checkmark"></div></td>
                                    <td>Some</td>
                                    <td><div class="icon-checkmark"></div></td>
                                    <td><div class="icon-checkmark"></div></td>
                                </tr>
                                <tr>
                                    <th scope="row">Cloud AI</th>
                                    <td><div class="icon-checkmark"></div></td>
                                    <td>Some</td>
                                    <td><div class="icon-checkmark"></div></td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <th scope="row">IoT Safeguard</th>
                                    <td><div class="icon-checkmark"></div></td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <th scope="row">Dynamic Support</th>
                                    <td><div class="icon-checkmark"></div></td>
                                    <td>-</td>
                                    <td><div class="icon-checkmark"></div></td>
                                    <td>-</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bizsecure-learn  fill-haze">
        <div class="container  container-slim  text-center  py-5">
            <div class="row  justify-content-center">
                <div class="col-md-8">
                    <h2 class="heading  h3  text-uppercase  mb-3">setting up your audra bizsecure</h2>
                    <div class="mb-5">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    </div>
                </div>
            </div>

            <div class="row  text-center">
                <div class="col">
                    <img src="assets/images/step-1-orange.png" alt="Step" class="d-block  mx-auto  mb-2"  width="48" height="auto" />
                    <div>Install a small yet robust Audra device</div>
                </div>
                <div class="col">
                    <img src="assets/images/step-2-orange.png" alt="Step" class="d-block  mx-auto  mb-2"  width="48" height="auto" />
                    <div>Download the Audra app</div>
                </div>
                <div class="col">
                    <img src="assets/images/step-3-orange.png" alt="Step" class="d-block  mx-auto  mb-2"  width="48" height="auto" />
                    <div>Set rules, as the app guides you step-by-step</div>
                </div>
                <div class="col">
                    <img src="assets/images/step-4-orange.png" alt="Step" class="d-block  mx-auto  mb-2"  width="48" height="auto" />
                    <div>Monitor, control and manage from your mobile app or desktop site</div>
                </div>
                <div class="col">
                    <img src="assets/images/step-5-orange.png" alt="Step" class="d-block  mx-auto  mb-2"  width="48" height="auto" />
                    <div>Enjoy peace of mind - anytime, anywhere</div>
                </div>
            </div>
        </div>
    </div>

    <div class="buynow-compatibility text-center">
        <h2 class="heading h3 text-uppercase pb-5">DEVICE COMPATIBILITY</h2>

        <div class="row text-center align-items-end pb-5 justify-content-md-center">
            <div class="col col-lg-3">
                <img src="assets/images/logo-unifi@2x.png" alt="Unifi" width="auto" height="66" />
            </div>
            <div class="col col-lg-3">
                <img src="assets/images/logo-maxis@2x.png" alt="Maxis" width="auto" height="75" />
            </div>
            <div class="col col-lg-3">
                <img src="assets/images/logo-timedotnet@2x.png" alt="TIME" width="auto" height="36" />
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
function incrementValue(e) {
    e.preventDefault();
    var fieldName = $(e.target).data('field');
    var parent = $(e.target).closest('div');
    var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

    if (!isNaN(currentVal)) {
        parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
    } else {
        parent.find('input[name=' + fieldName + ']').val(0);
    }
}

function decrementValue(e) {
    e.preventDefault();
    var fieldName = $(e.target).data('field');
    var parent = $(e.target).closest('div');
    var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

    if (!isNaN(currentVal) && currentVal > 0) {
        parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
    } else {
        parent.find('input[name=' + fieldName + ']').val(0);
    }
}

$('.input-group').on('click', '.button-plus', function(e) {
    incrementValue(e);
});

$('.input-group').on('click', '.button-minus', function(e) {
    decrementValue(e);
});
</script>

<?php include 'views/sections/footer.php'; ?>
<?php include 'views/templates/foot.php'; ?>
