<?php include 'views/templates/head.php'; ?>
<?php include 'views/sections/navbar.php'; ?>
<div class="wave" style="background-image: url('assets/images/wave-about-us.png')"></div>
<div class="content">
    <div class="container  container-slim">
        <h1 class="heading">Contact Us</h1>
        <?php include 'views/forms/contact-us.php'; ?>
    </div>
    <div class="container">
        <hr class="divider  divider-dashed">
    </div>
    <div class="container  container-slim  mt-5">
        <div class="lead  text-center">
            Need help troubleshooting your Audra device?
            <a href="#">Click here</a>.
        </div>
    </div>
</div>
<?php include 'views/sections/footer.php'; ?>
<?php include 'views/templates/foot.php'; ?>
