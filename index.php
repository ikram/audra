<?php include 'views/templates/head.php'; ?>
<?php include 'views/sections/navbar.php'; ?>
<div class="content">
    <div class="home-banners">
        <div id="banners" class="carousel  carousel-banners  slide  text-white" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <?php
                        $banner_wallpaper = "home-1.png";
                        $banner_title = "Safer Home Internet, More Playing Time";
                        $banner_story = "Protect loved ones and enhance digital wellness at home, with WiFi settings and security at your fingertips.";
                        $banner_action = "Let's Begin";
                        include 'views/carousel/banners-content.php';
                    ?>
                </div>
                <div class="carousel-item">
                    <?php
                        $banner_wallpaper = "home-2.png";
                        $banner_title = "Future-Proof Your Business";
                        $banner_story = "Grow your business, boost workplace productivity and data security with efficient WiFi management.";
                        $banner_action = "Let's Begin";
                        include 'views/carousel/banners-content.php';
                    ?>
                </div>
                <div class="carousel-item">
                    <?php
                        $banner_wallpaper = "home-3.png";
                        $banner_title = "Securing the connection with your customers";
                        $banner_story = "Accelerate customer satisfaction, optimise network risk management with a simple, plug-and-play solution.";
                        $banner_action = "Let's Begin";
                        include 'views/carousel/banners-content.php';
                    ?>
                </div>
            </div>

            <?php include 'views/carousel/banners-control.php'; ?>
        </div>
    </div>

    <div class="home-intro">
        <div class="container  container-slim  text-center">
            <h1 class="heading  text-uppercase  font-weight-normal">The Digital Storm</h1>
            <div class="row  justify-content-center">
                <div class="col-sm-8">
                    With the Internet as the core of the digital era, we are now more
                    connected than ever. As much as it has transformed our lives for
                    the better, it also opens up various risks that disrupt a normal,
                    healthy life. But life is what you make it, and so is your online
                    activity.
                </div>
            </div>

            <b><a href="#">Take charge today, with Audra</a></b>

            <div class="embed-responsive embed-responsive-16by9  mt-5">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/_74nDARLgOU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>

    <div class="home-products  home-products-first  position-relative">
        <img class="wallpaper" src="assets/images/wallpapers/wall-1.png" alt="wallpaper" />
        <div class="container  text-white">
            <div class="row">
                <div class="col-md-6">
                    <div id="home-products-1" class="carousel slide" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner">
                            <div class="carousel-item  active">
                                <div class="carousel-product">
                                    <div class="product-logo">
                                        <img src="assets/images/icon-homeshield-smartdevice.png">
                                    </div>
                                    <div class="product-tagline  h5  text-white  text-center">Manage Smart Devices</div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="carousel-product">
                                    <div class="product-logo">
                                        <img src="assets/images/icon-homeshield-parentalcontrol.png">
                                    </div>
                                    <div class="product-tagline  h5  text-white  text-center">Parental Control</div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="carousel-product">
                                    <div class="product-logo">
                                        <img src="assets/images/icon-homeshield-homedigital.png">
                                    </div>
                                    <div class="product-tagline  h5  text-white  text-center">Your Home's Digital Wellness</div>
                                </div>
                            </div>
                        </div>

                        <a class="carousel-control-prev" href="#home-products-1" role="button" data-slide="prev">
                            <svg class="icon-chevron-back" width="48" height="48" fill="currentColor"><use xlink:href="#icon-chevron-back"></use></svg>
                        </a>

                        <a class="carousel-control-next" href="#home-products-1" role="button" data-slide="next">
                            <svg class="icon-chevron-next" width="48" height="48" fill="currentColor"><use xlink:href="#icon-chevron-next"></use></svg>
                        </a>
                    </div>
                </div>
                <div class="col-md-6">
                    <?php
                        $product_name       = 'Audra HomeShield';
                        $product_tagline    = 'Protect & Regulate';
                        $product_story      = 'Audra HomeShield enables you to set and manage WiFi accessibility for all devices connected at home, no matter where you are.';
                        $product_action     = 'Make Home a Safer Place';
                    ?>
                    <?php include 'views/carousel/product-content.php'; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="home-products  home-products-second  position-relative">
        <img class="wallpaper" src="assets/images/wallpapers/wall-2.png" alt="wallpaper" />
        <div class="container  text-white">
            <div class="row">
                <div class="col-md-6  order-md-2">
                    <div id="home-products-2" class="carousel slide" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner">
                            <div class="carousel-item  active">
                                <div class="carousel-product">
                                    <div class="product-logo">
                                        <img src="assets/images/icon-bizsecure-reducedowntime.png">
                                    </div>
                                    <div class="product-tagline  h5  text-white  text-center">Reduce Business Downtown</div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="carousel-product">
                                    <div class="product-logo">
                                        <img src="assets/images/icon-bizsecure-productivity.png">
                                    </div>
                                    <div class="product-tagline  h5  text-white  text-center">Enhance Productivity</div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="carousel-product">
                                    <div class="product-logo">
                                        <img src="assets/images/icon-bizsecure-datatheft.png">
                                    </div>
                                    <div class="product-tagline  h5  text-white  text-center">Safeguard Data Theft</div>
                                </div>
                            </div>
                        </div>

                        <a class="carousel-control-prev" href="#home-products-2" role="button" data-slide="prev">
                            <svg class="icon-chevron-back" width="48" height="48" fill="currentColor"><use xlink:href="#icon-chevron-back"></use></svg>
                        </a>

                        <a class="carousel-control-next" href="#home-products-2" role="button" data-slide="next">
                            <svg class="icon-chevron-next" width="48" height="48" fill="currentColor"><use xlink:href="#icon-chevron-next"></use></svg>
                        </a>
                    </div>
                </div>
                <div class="col-md-6  order-md-1">
                    <?php
                        $product_name       = 'Audra BizSecure';
                        $product_tagline    = 'SECURE & ENHANCE';
                        $product_story      = 'Audra BizSecure gives you absolute authority and real time control over your official wireless network, and Internet access points within your workplace.';
                        $product_action     = 'Experience Higher Productivity Now';
                    ?>
                    <?php include 'views/carousel/product-content.php'; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="home-testimonial">
        <div class="container  text-center">
            <h2 class="heading  text-dark  mb-5">Let Our Client Do The Talking</h2>
            <div class="row d-flex justify-content-center align-items-xl-center">
              <div class="home-testimonial-card">
                <blockquote class="m-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</blockquote>
                <div class="d-flex justify-content-center"><img src="assets/images/sony_logo_PNG2.png"></div>
              </div>
              <div class="home-testimonial-card card-active">
                <blockquote class="m-3">..works buttery<br>smooth, support<br>team is great..</blockquote>
                <div class="d-flex justify-content-center"><img src="assets/images/sony_logo_PNG2.png"></div>
              </div>
              <div class="home-testimonial-card">
                <blockquote class="m-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</blockquote>
                <div class="d-flex justify-content-center"><img src="assets/images/sony_logo_PNG2.png"></div>
              </div>
            </div>
            <!-- <?php include 'views/carousel/banners-control.php'; ?> -->
          </div>
        </div>
    </div>

    <div class="home-featured">
        <div class="container  text-center">
            <h3 class="heading  text-dark  font-weight-normal  mb-5">We're Also Featured Here</h3>
            <div class="row">
                <?php for($featured=0; $featured<6; $featured++ ) { ?>
                    <div class="col-6  col-md-2">
                        <div class="home-featured-logo"></div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php include 'views/sections/footer.php'; ?>
<?php include 'views/templates/foot.php'; ?>
