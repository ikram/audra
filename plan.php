<?php include 'views/templates/head.php'; ?>
<?php include 'views/sections/navbar.php'; ?>
<div class="content">
    <div class="container  py-5">
        <h4>Working Plan</h4>
        <p>
            We have a lot to cover in a short time.
            Focus on simple pages HTML/CSS templating first then kickoff the WordPress theming.
        </p>
        <p>
            Below is pages that we should accomplished, green marked as 'simple page':
        </p>
        <ol>
            <li class="text-success">About Us</li>
            <li>Audra Cloud</li>
            <li>BiZSecure</li>
            <li>BuyNow</li>
            <li class="text-success">Contact Us</li>
            <li class="text-success">Digital Wellness</li>
            <li>Home</li>
            <li>HomeShield</li>
            <li>ISP</li>
            <li class="text-success">Media</li>
            <li class="text-success">Partner</li>
        </ol>
        <p>
            The simple pages should be complete by Thu (June 12).
        </p>
        <hr class="divider">
        <p>
            The other pages HTML/CSS templates should be complete over the weekend and hopefully we can convert it to WordPress pages too.
        </p>
    </div>
</div>
<?php include 'views/sections/footer.php'; ?>
<?php include 'views/templates/foot.php'; ?>
