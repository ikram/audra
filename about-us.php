<?php include 'views/templates/head.php'; ?>
<?php include 'views/sections/navbar.php'; ?>
<div class="wave" style="background-image: url('assets/images/wave-about-us.png')"></div>
<div class="content">
    <div class="about-intro">
        <div class="container  container-slim">
            <h1 class="heading">About Us</h1>
            <div class="about-brief">
                Dotlines is a technology powered consumer and business solution
                group, headquartered in Singapore, working across Asia for more than
                15 years. The group works along 10+ business verticals to bring
                smiles to 7+ million consumers. Dotlines aspires to ignite
                possibilities, by solving tangible problems and impacting lives
                through use of technology enabled services. It will always make
                complex technology simple for the wider mass and eventually
                make life better and larger.
            </div>
            <div class="about-services  text-center">
                <div class="row">
                    <div class="col-md-4  py-4">
                        <div class="service">
                            <div class="service-icon" style="background-image: url('assets/images/icon-about-internet.png');"></div>
                            <div class="service-label">Internet Connectivity</div>
                        </div>
                    </div>
                    <div class="col-md-4  py-4">
                        <div class="service">
                            <div class="service-icon" style="background-image: url('assets/images/icon-about-security.png');"></div>
                            <div class="service-label">Digital security & services</div>
                        </div>
                    </div>
                    <div class="col-md-4  py-4">
                        <div class="service">
                            <div class="service-icon" style="background-image: url('assets/images/icon-about-education.png');"></div>
                            <div class="service-label">Distance Education</div>
                        </div>
                    </div>
                    <div class="col-md-4  py-4">
                        <div class="service">
                            <div class="service-icon" style="background-image: url('assets/images/icon-about-employment.png');"></div>
                            <div class="service-label">Employment</div>
                        </div>
                    </div>
                    <div class="col-md-4  py-4">
                        <div class="service">
                            <div class="service-icon" style="background-image: url('assets/images/icon-about-empowerment.png');"></div>
                            <div class="service-label">Empowerment</div>
                        </div>
                    </div>
                    <div class="col-md-4  py-4">
                        <div class="service">
                            <div class="service-icon" style="background-image: url('assets/images/icon-about-insurance.png');"></div>
                            <div class="service-label">Micro Insurance</div>
                        </div>
                    </div>
                    <div class="col-md-4  py-4">
                        <div class="service">
                            <div class="service-icon" style="background-image: url('assets/images/icon-about-payment.png');"></div>
                            <div class="service-label">Payments & Remitances</div>
                        </div>
                    </div>
                    <div class="col-md-4  py-4">
                        <div class="service">
                            <div class="service-icon" style="background-image: url('assets/images/icon-about-logistic.png');"></div>
                            <div class="service-label">Logistics</div>
                        </div>
                    </div>
                    <div class="col-md-4  py-4">
                        <div class="service">
                            <div class="service-icon" style="background-image: url('assets/images/icon-about-infotainment.png');"></div>
                            <div class="service-label">Digital Infotaiment</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="about-map">
        <div class="container">
            <div class="d-flex  align-items-center  justify-content-center">
                <img src="assets/images/about-us-map.png" width="100%" height="auto">
            </div>
        </div>
    </div>
    <div class="about-statistics">
        <div class="container  text-center">
            <div class="row">
                <div class="col-6  col-md-4  d-flex  justify-content-center  py-4">
                    <div class="statistic  rounded-circle  d-flex  align-items-center  justify-content-center">
                        <div>
                            <div class="statistic-number">15+</div>
                            <div class="statistic-label">Year of<br/>Experience</div>
                        </div>
                    </div>
                </div>
                <div class="col-6  col-md-4  d-flex  justify-content-center  py-4">
                    <div class="statistic  rounded-circle  d-flex  align-items-center  justify-content-center">
                        <div>
                            <div class="statistic-number">8</div>
                            <div class="statistic-label">Country<br/>Footprint</div>
                        </div>
                    </div>
                </div>
                <div class="col-6  col-md-4  d-flex  justify-content-center  py-4">
                    <div class="statistic  rounded-circle  d-flex  align-items-center  justify-content-center">
                        <div>
                            <div class="statistic-number">10+</div>
                            <div class="statistic-label">Business<br>Concerns</div>
                        </div>
                    </div>
                </div>
                <div class="col-6  col-md-4  d-flex  justify-content-center  py-4">
                    <div class="statistic  rounded-circle  d-flex  align-items-center  justify-content-center">
                        <div>
                            <div class="statistic-number">630</div>
                            <div class="statistic-label">Employees</div>
                        </div>
                    </div>
                </div>
                <div class="col-6  col-md-4  d-flex  justify-content-center  py-4">
                    <div class="statistic  rounded-circle  d-flex  align-items-center  justify-content-center">
                        <div>
                            <div class="statistic-number">2</div>
                            <div class="statistic-label">Development<br />Center</div>
                        </div>
                    </div>
                </div>
                <div class="col-6  col-md-4  d-flex  justify-content-center  py-4">
                    <div class="statistic  rounded-circle  d-flex  align-items-center  justify-content-center">
                        <div>
                            <div class="statistic-number">7+</div>
                            <div class="statistic-label">Million<br />Consumers</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'views/sections/footer.php'; ?>
<?php include 'views/templates/foot.php'; ?>
