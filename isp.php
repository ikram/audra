<?php include 'views/templates/head.php'; ?>
<?php include 'views/sections/navbar.php'; ?>
<div class="content">
    <div class="isp-banners">
        <div id="banners" class="carousel  carousel-banners  slide  text-white" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <?php
                        $banner_title = '360 cloud-based security solution for your network and customers';
                        $banner_story = "Secure not just your customers' data, but also service satisfaction with stress-free customer support and network downtime prevention.";
                        include 'views/carousel/banners-content.php';
                    ?>
                </div>
                <div class="carousel-item">
                    <?php
                        $banner_title = 'Disruption-free browsing experience at all times';
                        $banner_story = 'Enjoy quick response rates and uninterrupted internet usage while staying protected.';
                        include 'views/carousel/banners-content.php';
                    ?>
                </div>
                <div class="carousel-item">
                    <?php
                        $banner_title = 'Secured & Encrypted Data for Peace of Mind';
                        $banner_story = "Audra manages your data's identity and prevents breach and data theft from unrecognize parties that disrupt your devices.";
                        include 'views/carousel/banners-content.php';
                    ?>
                </div>
            </div>

            <?php include 'views/carousel/banners-control.php'; ?>
        </div>
    </div>

    <div class="isp-intro">
        <div class="container  container-slim">
            <div class="row">
                <div class="col-md-5">
                    <h1 class="h2  heading  text-uppercase  font-weight-normal">Better Security And More Control</h1>
                    <div class="mb-5">
                        Prevent network downtime, revenue loss, low CSAT, stressed customer support and attrition on the daily. Audra's simple plug-and-play solution keeps your network and customers protected from cyber attacks, complete with customizable internet usage preferences.
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/_74nDARLgOU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>

            <div class="row  align-items-center  mt-5">
                <div class="col-md-3  order-md-12">
                    <div class="heading  text-primary  h4  m-0">Bulletproof Your Network Against Threats</div>
                </div>

                <div class="col-md-9">
                    <div class="isp-stats  text-center">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="d-flex  justify-content-center">
                                    <div class="stat  rounded-circle  d-flex  align-items-center  justify-content-center">
                                        <div class="stat-content">
                                            <div class="stat-label"><strong>DDoS</strong><br/>attacks</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="d-flex  justify-content-center">
                                    <div class="stat  rounded-circle  d-flex  align-items-center  justify-content-center">
                                        <div class="stat-content">
                                            <div class="stat-label">Infrastructural<br/><strong>outage</strong></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="d-flex  justify-content-center">
                                    <div class="stat  rounded-circle  d-flex  align-items-center  justify-content-center">
                                        <div class="stat-content">
                                            <div class="stat-label"><strong>Bandwidth</strong><br/>congestion</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="d-flex  justify-content-center">
                                    <div class="stat  rounded-circle  d-flex  align-items-center  justify-content-center">
                                        <div class="stat-content">
                                            <div class="stat-label"><strong>Route</strong><br/>hijacking</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="isp-how  fill-haze">
        <div class="container  container-slim  text-center">
            <div class="row  justify-content-center">
                <div class="col-md-8">
                    <h1 class="heading  h3  text-uppercase  font-weight-normal">HOW AUDRA SAFEGUARDS YOUR NETWORK</h1>
                    <div class="mb-5">
                        Audra prevents threats by identifying sources of malicious activities within your network, down to individual devices. It also allows your customers to set rules, manage internet usage within their environment, filter websites, and receive real-time network notifications.
                    </div>
                </div>
            </div>

            <div style="border: 1px dashed #ddd; text-align: center; padding: 300px 0;">Map Here</div>
        </div>
    </div>
</div>
<?php include 'views/sections/footer.php'; ?>
<?php include 'views/templates/foot.php'; ?>
