<?php include 'views/templates/head.php'; ?>
<?php include 'views/sections/navbar.php'; ?>
<div class="wave" style="background-image: url('assets/images/wave-about-us.png')"></div>
<div class="content">
    <div class="container  container-slim">
        <h1 class="heading">Be an Audra Partner</h1>
        <p class="lead">Join us as an Audra Partner and be a part of a diverse group focused on driving innovation and designing empowering technology services.</p>
        <h2 class="heading">Why Partner With Us</h2>
        <div class="row">
            <div class="col-md-6">
                <h5 class="m-0">Empower your business while making a change</h5>
                <p>
                    We believe that we have a part to play in impacting life
                    and transforming it for the better, with the touch of
                    empowering techonology.
                </p>
                <p>
                    As a partner you will have access to our innovative suite
                    of products that are designed to drive both profitability
                    and communal betterment.
                </p>
            </div>
            <div class="col-md-6">
                <h5 class="m-0">Be part of diverse global network</h5>
                <p>
                    Spanning across 10 countries, with 600+ employees from
                    5 different nationalities, the group instils diversity in
                    its deep insights and practical innovations.
                </p>
                <p>
                    With offices in Malaysia, Singapore and working across Asia,
                    the Middle East and the Americas, our network is able to
                    provide unsurpassed partner support wherever and whenever
                    you ned it.
                </p>
            </div>
        </div>
    </div>
    <div class="container">
        <hr class="divider  divider-dashed">
    </div>
    <div class="container  container-slim">
        <h2 class="heading">Contact Us</h2>
        <?php include 'views/forms/contact-us.php'; ?>
    </div>
</div>
<?php include 'views/sections/footer.php'; ?>
<?php include 'views/templates/foot.php'; ?>
