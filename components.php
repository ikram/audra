<?php include 'views/templates/head.php'; ?>
<?php include 'views/sections/navbar.php'; ?>
<div class="content">
    <div class="home-testimonial">
        <div class="container  text-center">
            <h2 class="heading  text-dark  mb-5">Let Our Client Do The Talking</h2>
            <div class="row d-flex justify-content-center align-items-xl-center">
                <div class="home-testimonial-card">
                    <blockquote class="m-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</blockquote>
                    <div class="d-flex justify-content-center"><img src="assets/images/sony_logo_PNG2.png"></div>
                </div>
                <div class="home-testimonial-card card-active">
                    <blockquote class="m-3">..works buttery<br>smooth, support<br>team is great..</blockquote>
                    <div class="d-flex justify-content-center"><img src="assets/images/sony_logo_PNG2.png"></div>
                </div>
                <div class="home-testimonial-card">
                    <blockquote class="m-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</blockquote>
                    <div class="d-flex justify-content-center"><img src="assets/images/sony_logo_PNG2.png"></div>
                </div>
            </div>
            <!-- <?php include 'views/carousel/banners-control.php'; ?> -->
        </div>
    </div>
</div>

<!--
Put into: Homepage

Apply ACF if got more times.
-->
<div class="home-featured  py-6">
    <div class="container  text-center">
        <h3 class="heading  text-dark  font-weight-normal  mb-5">We're Also Featured Here</h3>
        <div class="home-featured-logos">
            <div class="d-inline-block"><img class="d-block" src="assets/images/logo-featured-bi.png" height="88" width="auto" /></div>
            <div class="d-inline-block"><img class="d-block" src="assets/images/logo-featured-citacita.png" height="88" width="auto" /></div>
            <div class="d-inline-block"><img class="d-block" src="assets/images/logo-featured-mf.png" height="88" width="auto" /></div>
            <div class="d-inline-block"><img class="d-block" src="assets/images/logo-featured-mobile88.png" height="88" width="auto" /></div>
            <div class="d-inline-block"><img class="d-block" src="assets/images/logo-featured-qoshe.png" height="88" width="auto" /></div>
        </div>
    </div>
</div>



<!--Apply ACF-->
<div class="{isp/cloud/bizsecure/homeshield}-how  fill-haze  py-6">
    <div class="container  container-slim  text-center">
        <div class="row  justify-content-center">
            <div class="col-md-8">
                <h1 class="heading  h3  text-uppercase  font-weight-normal">Title</h1>
                <div class="mb-5">
                    Audra prevents threats by identifying sources of malicious activities within your network, down to individual devices. It also allows your customers to set rules, manage internet usage within their environment, filter websites, and receive real-time network notifications.
                </div>
            </div>
        </div>

        <div class="d-block  d-sm-none">
            <!--Diagram: Mobile Version-->
            <img src="assets/images/diagram/diagram-homeshield-mobile.png" width="100%" height="auto" />
        </div>
        <div class="d-none  d-sm-block">
            <!--Diagram: Desktop & Tablet Version-->
            <img src="assets/images/diagram/diagram-homeshield.png" width="100%" height="auto" />
        </div>
    </div>
</div>
<?php include 'views/sections/footer.php'; ?>
<?php include 'views/templates/foot.php'; ?>
