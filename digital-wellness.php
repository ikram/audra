<?php include 'views/templates/head.php'; ?>
<?php include 'views/sections/navbar.php'; ?>
<div class="wave" style="background-image: url('assets/images/wave-about-us.png')"></div>
<div class="content">
    <div class="container  container-slim">
        <h1 class="heading">Digital Wellness</h1>
        <h5>Achieve an optimum state of balance between your tech and non-tech lives.</h5>

        <div>
            There is a growing cadre of experts from across a variety of
            disciplines, and across the globe, who are beginning to think about
            these issues. The need is emerging for a new field of Digital
            Wellness that strikes a balanced relationship between technology and
            wellbeing and seeks to establish a holistic and unified approach.
        </div>

        <div class="digital-services  text-center">
            <div class="row">
                <div class="col-md-6  mb-4">
                    <div class="digital">
                        <div class="digital-icon mb-3"></div>
                        <h3 class="digital-title  h5  m-0">Digital Education</h3>
                        <div class="digital-story">
                            It is when learning is accompanied by technology or
                            by instructional practice that makes effective use
                            of technology. It encompasses the application of a
                            wide spectrum of practices including blended and
                            virtual learning.
                        </div>
                        <hr class="divider  divider-dashed  mt-auto" />
                    </div>
                </div>
                <div class="col-md-6  mb-4">
                    <div class="digital">
                        <div class="digital-icon mb-3"></div>
                        <h3 class="digital-title  h5  m-0">Digital Nutrition</h3>
                        <div class="digital-story">
                            It explores a range of social, emotional and cognitive
                            impacts of our technology use (and overuse!) and
                            provides solutions to help maximise the benefits of
                            devices and avoid the pitfalls. It’s for parents,
                            students, educators – or any human using technology
                            seeking to understand how our digital habits are
                            shaping our brains and behaviours.
                        </div>
                        <hr class="divider  divider-dashed  mt-auto" />
                    </div>
                </div>
                <div class="col-md-6  mb-4">
                    <div class="digital">
                        <div class="digital-icon mb-3"></div>
                        <h3 class="digital-title  h5  m-0">Digital Mindfulness</h3>
                        <div class="digital-story">
                            It is the practice of paying attention to your digital
                            experiences, maintaining a moment-by-moment awareness
                            of your thoughts, feelings, physical sensations, and
                            surroundings, without judgment. This awareness fosters
                            interactions with technology that complement human
                            nature, and inspire global society to a new
                            consciousness.
                        </div>
                        <hr class="divider  divider-dashed  mt-auto" />
                    </div>
                </div>
                <div class="col-md-6  mb-4">
                    <div class="digital">
                        <div class="digital-icon mb-3"></div>
                        <h3 class="digital-title  h5  m-0">Digital Citizenship</h3>
                        <div class="digital-story">
                            A concept which concerns itself with helping students
                            to learn, communicate and collaborate safely and
                            responsibly as a digital citizen. This includes
                            having email etiquette, reporting and preventing
                            cyber bullying, learning how to protect private
                            information, etc.
                        </div>
                        <hr class="divider  divider-dashed  mt-auto" />
                    </div>
                </div>
                <div class="col-md-6  mb-4">
                    <div class="digital">
                        <div class="digital-icon mb-3"></div>
                        <h3 class="digital-title  h5  m-0">Digital Ethics</h3>
                        <div class="digital-story">
                            The idea of how to manage oneself ethically,
                            professionally and in a clinically sound manner via
                            online and digital mediums.
                        </div>
                        <hr class="divider  divider-dashed  mt-auto" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'views/sections/footer.php'; ?>
<?php include 'views/templates/foot.php'; ?>
