<?php include 'views/templates/head.php'; ?>
<?php include 'views/sections/navbar.php'; ?>
<div class="content">
    <div class="homeshield-banners">
        <div id="banners" class="carousel  carousel-banners  slide  text-white" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <?php
                        $banner_title = "Secure your business's most valuable asset";
                        $banner_story = "Protect your connected devices from malware, adware, and cyberattacks, keeping your data confidential and safe. ";
                        include 'views/carousel/banners-content.php';
                    ?>
                </div>
                <div class="carousel-item">
                    <?php
                        $banner_title = 'Disruption-free browsing experience at all times';
                        $banner_story = 'Enjoy quick response rates and uninterrupted internet usage while staying protected.';
                        include 'views/carousel/banners-content.php';
                    ?>
                </div>
                <div class="carousel-item">
                    <?php
                        $banner_title = 'Secured & Encrypted Data for Peace of Mind';
                        $banner_story = "Audra manages your data's identity and prevents breach and data theft from unrecognize parties that disrupt your devices.";
                        include 'views/carousel/banners-content.php';
                    ?>
                </div>
            </div>

            <?php include 'views/carousel/banners-control.php'; ?>
        </div>
    </div>

    <div class="homeshield-intro">
        <div class="container  container-slim">
            <div class="row">
                <div class="col-md-5">
                    <h1 class="h2  heading  text-uppercase">PRODUCTIVITY HAS NEVER BEEN SO EASY</h1>
                    <div class="mb-5">
                        With the Internet as the core of the digital era, we are now more connected than ever. As much as it has transformed our lives for the better, it also opens up various risks that disrupt a normal, healthy life. But life is what you make it, and so is your online activity.
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/_74nDARLgOU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>

            <div class="row  align-items-center  mt-5">
                <div class="col-md-3  order-md-12">
                    <div class="heading  text-primary  h4  m-0">SAFEGUARD YOUR BUSINESS AGAINST THESE ISSUES</div>
                </div>

                <div class="col-md-9">
                    <div class="homeshield-stats  text-center">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="d-flex  justify-content-center">
                                    <div class="stat  rounded-circle  d-flex  align-items-center  justify-content-center">
                                        <div class="stat-content">
                                            <div class="stat-count">$2.4M</div>
                                            <div class="stat-label">average loss per malware infection</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="d-flex  justify-content-center">
                                    <div class="stat  rounded-circle  d-flex  align-items-center  justify-content-center">
                                        <div class="stat-content">
                                            <div class="stat-count">41%</div>
                                            <div class="stat-label">businesses had downtime due to attack</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="d-flex  justify-content-center">
                                    <div class="stat  rounded-circle  d-flex  align-items-center  justify-content-center">
                                        <div class="stat-content">
                                            <div class="stat-count">58%</div>
                                            <div class="stat-label">malware attack victims are SMBs</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="d-flex  justify-content-center">
                                    <div class="stat  rounded-circle  d-flex  align-items-center  justify-content-center">
                                        <div class="stat-content">
                                            <div class="stat-count">600%</div>
                                            <div class="stat-label">increase in attacks against IoT devices</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="homeshield-how  fill-haze">
        <div class="container  container-slim  text-center  py-5">
            <div class="row  justify-content-center">
                <div class="col-md-8">
                    <h2 class="heading  h3  text-uppercase">how audra bizsecure safeguards your business</h2>
                    <div class="mb-5">
                        Audra BizSecure ensures that your business is seamlessly protected against dangers of the internet with its simple plug-and-play setup. It allows you to set the rules of your internet to keep threats like unmonitored browsing, unregulated screen time, malwares, adwares and hackers, away from the limits of your business network to enjoy peace of mind.
                    </div>
                </div>
            </div>

            <div style="border: 1px dashed #ddd; text-align: center; padding: 300px 0;">Map Here</div>
        </div>
    </div>

    <div class="homeshield-learn">
        <div class="container  container-slim  text-center  py-5">
            <div class="row  justify-content-center">
                <div class="col-md-8">
                    <h2 class="heading  h3  text-uppercase  mb-5">LEARN MORE ABOUT AUDRA BIZSecure</h2>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/_74nDARLgOU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/_74nDARLgOU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="homeshield-learn  fill-haze">
        <div class="container  container-slim  text-center  py-5">
            <div class="row  justify-content-center">
                <div class="col-md-8">
                    <h2 class="heading  h3  text-uppercase  mb-3">setting up your audra bizsecure</h2>
                    <div class="mb-5">
                        Staying true to its plug-and-play nature, setting up your Audra HomeShield is simple and hassle-free. Your journey towards a harmonius balance between technology and personal wellbeing starts here.
                    </div>
                </div>
            </div>

            <div class="row  text-center">
                <div class="col">
                    <img src="assets/images/step-1-orange.png" alt="Step" class="d-block  mx-auto  mb-2"  width="48" height="auto" />
                    <div>Install a small yet robust Audra device</div>
                </div>
                <div class="col">
                    <img src="assets/images/step-2-orange.png" alt="Step" class="d-block  mx-auto  mb-2"  width="48" height="auto" />
                    <div>Download the Audra app</div>
                </div>
                <div class="col">
                    <img src="assets/images/step-3-orange.png" alt="Step" class="d-block  mx-auto  mb-2"  width="48" height="auto" />
                    <div>Set rules, as the app guides you step-by-step</div>
                </div>
                <div class="col">
                    <img src="assets/images/step-4-orange.png" alt="Step" class="d-block  mx-auto  mb-2"  width="48" height="auto" />
                    <div>Monitor, control and manage from your mobile app or desktop site</div>
                </div>
                <div class="col">
                    <img src="assets/images/step-5-orange.png" alt="Step" class="d-block  mx-auto  mb-2"  width="48" height="auto" />
                    <div>Enjoy peace of mind - anytime, anywhere</div>
                </div>
            </div>

            <h2 class="heading  h3  text-uppercase  mb-3  mt-5">DEVICE COMPATIBILITY</h2>

            <div class="row  text-center  align-items-end">
                <div class="col">
                    <img src="assets/images/logo-unifi.png" alt="Unifi" width="auto" height="88" />
                </div>
                <div class="col">
                    <img src="assets/images/logo-maxis.png" alt="Maxis" width="auto" height="100" />
                </div>
                <div class="col">
                    <img src="assets/images/logo-time.png" alt="TIME" width="auto" height="48" />
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'views/sections/footer.php'; ?>
<?php include 'views/templates/foot.php'; ?>
