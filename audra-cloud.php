<?php include 'views/templates/head.php'; ?>
<?php include 'views/sections/navbar.php'; ?>
<div class="content">
    <div class="audracloud-banners">
        <div id="banners" class="carousel  carousel-banners  slide  text-white" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <?php
                        $banner_title = 'Filtered Web Traffic to Prevent Unwanted Access';
                        $banner_story = 'Ensure traffic security via our gateway before it reaches devices connected to your network.';
                        include 'views/carousel/banners-content.php';
                    ?>
                </div>
                <div class="carousel-item">
                    <?php
                        $banner_title = 'Disruption-free browsing experience at all times';
                        $banner_story = 'Enjoy quick response rates and uninterrupted internet usage while staying protected.';
                        include 'views/carousel/banners-content.php';
                    ?>
                </div>
                <div class="carousel-item">
                    <?php
                        $banner_title = 'Secured & Encrypted Data for Peace of Mind';
                        $banner_story = "Audra manages your data's identity and prevents breach and data theft from unrecognize parties that disrupt your devices.";
                        include 'views/carousel/banners-content.php';
                    ?>
                </div>
            </div>

            <?php include 'views/carousel/banners-control.php'; ?>
        </div>
    </div>

    <div class="audracloud-intro">
        <div class="container  container-slim">
            <div class="row">
                <div class="col-md-5">
                    <h1 class="h2  heading  text-uppercase  font-weight-normal">Intelligence for Seamless Protection</h1>
                    <div class="mb-5">
                        Audra Cloud brings you security, safety, and digital wellness through state-of-the-art AI-driven gateway solutions. It filters information and restricts unwarranted access to keep your data safe from external breach, while allowing you to control and set preferences from your fingertips.
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/_74nDARLgOU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="audracloud-how  fill-haze">
        <div class="container  container-slim  text-center">
            <div class="row  justify-content-center">
                <div class="col-md-8">
                    <h1 class="heading  h3  text-uppercase  font-weight-normal">HOW AUDRA CLOUD ENSURES SECURITY AND SAFETY WITHOUT SLOWING YOU DOWN</h1>
                    <div class="mb-5">
                        Audra allows all traffic to pass through your physical unit, unless restricted by rules set by you, which it validates with Audra Cloud before allowing or rejecting traffic. Policies and rules are synced via physical unit at regular intervals to ensure seamless response time and quick browsing experience.
                    </div>
                </div>
            </div>

            <div style="border: 1px dashed #ddd; text-align: center; padding: 300px 0;">Map Here</div>
        </div>
    </div>
</div>
<?php include 'views/sections/footer.php'; ?>
<?php include 'views/templates/foot.php'; ?>
