<?php include 'views/templates/head.php'; ?>
<?php include 'views/sections/navbar.php'; ?>
<div class="wave" style="background-image: url('assets/images/wave-media.png')"></div>
<div class="content">
  <div class="media-body">
    <div class="container  container-slim">
        <h1 class="heading">Media</h1>

        <div class="media-navigator mb-5">
          <div class="btn-group mr-3">
            <button type="button" class="btn btn-sm btn-light dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Year
              <svg class="icon-chevron-next rotate" width="24" height="24" fill="currentColor"><use xlink:href="#icon-chevron-next"></use></svg>
            </button>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="#">2019</a>
              <a class="dropdown-item" href="#">2018</a>
              <a class="dropdown-item" href="#">2017</a>
            </div>
          </div>

          <button type="button" class="btn btn-sm btn-info">
            SEARCH
            <svg class="icon-chevron-next" width="24" height="24" fill="currentColor"><use xlink:href="#icon-chevron-next"></use></svg>
            <!-- <svg class="icon-forward" width="40" height="40" fill="currentColor"><use xlink:href="#icon-forward"></use></svg> -->
          </button>
        </div>

        <div class="row">
          <div class="media-item col-sm">
            <h4 class="text-white">Why are tech titans like Google and Apple talking about digital wellness?</h4>
            <span> Asian Correspondent</span>
            <time>August 8, 2018</time>
          </div>

          <div class="media-item col-sm">
            <h4 class="text-white">Why are tech titans like Google and Apple talking about digital wellness?</h4>
            <span> Asian Correspondent</span>
            <time>August 8, 2018</time>
          </div>

          <div class="w-100"></div>

          <div class="media-item col-sm">
            <h4 class="text-white">Put Down that Phone – Digital Wellness has Arrived and Here’s What You Should Know About It</h4>
            <span> Asian Correspondent</span>
            <time>August 8, 2018</time>
          </div>

          <div class="media-item col-sm">
            <h4 class="text-white">Digital Wellness has arrived and here’s what you should know about it</h4>
            <span> Asian Correspondent</span>
            <time>August 8, 2018</time>
          </div>

          <div class="w-100"></div>

          <div class="media-item col-sm">
            <h4 class="text-white">Why are tech titans like Google and Apple talking about digital wellness?</h4>
            <span> Asian Correspondent</span>
            <time>August 8, 2018</time>
          </div>

          <div class="media-item col-sm">
            <h4 class="text-white">Why are tech titans like Google and Apple talking about digital wellness?</h4>
            <span> Asian Correspondent</span>
            <time>August 8, 2018</time>
          </div>
        </div>

        <h3 class="heading h3 text-uppercase text-center pt-3 pb-5"><a href="#">LOAD ALL</a></h3>

    </div>
  </div>

    <div class="media-kit">
      <h1 class="heading text-uppercase p-5 text-white text-center">Media Kit</h1>

      <div class="row text-center align-items-end pb-5 justify-content-md-center">
          <div class="col col-lg-3">
              <img src="assets/images/logo-audra-color.png" alt="logo-audra-color" class="mb-3" width="auto" height="180" />
              <span>Audra Logo Color</span>
              <a href="#"><img src="assets/images/icon-pdf.png" width="30px" height="auto"/></a>
          </div>
          <div class="col col-lg-3">
              <img src="assets/images/logo-audra-white.png" alt="logo-audra-white" class="mb-3" width="auto" height="180" />
              <span>Audra Logo Black & White</span>
              <a href="#"><img src="assets/images/icon-pdf.png" width="30px" height="auto"/></a>
          </div>
          <div class="col col-lg-3">
              <img src="assets/images/logo-audra-single.png" alt="logo-audra-single" class="mb-3" width="auto" height="180" />
              <span>Audra Logo Single Color</span>
              <a href="#"><img src="assets/images/icon-pdf.png" width="30px" height="auto"/></a>
          </div>
      </div>
    </div>
</div>
<?php include 'views/sections/footer.php'; ?>
<?php include 'views/templates/foot.php'; ?>
