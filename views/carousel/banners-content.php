<?php
    $banner_title       = isset($banner_title) ? $banner_title : 'Banner Name';
    $banner_story       = isset($banner_story) ? $banner_story : 'Banner Story';
    $banner_action      = isset($banner_action) ? $banner_action : '';
    $banner_wallpaper   = isset($banner_wallpaper) ? $banner_wallpaper : 'home-1.png';
?>
<img class="wallpaper" src="assets/images/banners/<?=$banner_wallpaper?>" alt="Wallpaper">
<div class="container  position-relative">
    <div class="row">
        <div class="col-md-7">
            <div class="banner-space"></div>
        </div>
        <div class="col-md-5">
            <div class="banner-content  d-flex  align-items-center">
                <img class="curve" src="assets/images/banners/curve-green.png" alt="Curve">
                <div>
                    <h2 class="banners-title  h1  mb-2  text-white  text-uppercase"><?=$banner_title?></h2>
                    <div class="banners-brief  d-none  d-md-block  text-white"><?=$banner_story?></div>
                    <?php if($banner_action) { ?>
                    <div class="banners-action  mt-5">
                        <a href="#" class="btn  btn-pill">
                            <span><?=$banner_action?></span>
                            <svg class="icon-chevron-next" width="24" height="24" fill="currentColor"><use xlink:href="#icon-chevron-next"></use></svg>
                        </a>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
