<div class="carousel-media">
    <a class="carousel-control-prev" href="#<?=$carousel_id?>" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>

    <div class="carousel-product">
        <div class="carousel-product-logo"></div>
        <div class="carousel-product-tagline  text-white  text-center">Tagline</div>
    </div>

    <a class="carousel-control-next" href="#<?=$carousel_id?>" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
