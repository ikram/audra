<div class="banners-control  container  position-relative">
    <a class="carousel-control-prev" href="#banners" role="button" data-slide="prev">
        <svg class="icon-chevron-back" width="48" height="48" fill="currentColor"><use xlink:href="#icon-chevron-back"></use></svg>
    </a>
    <a class="carousel-control-next" href="#banners" role="button" data-slide="next">
        <svg class="icon-chevron-next" width="48" height="48" fill="currentColor"><use xlink:href="#icon-chevron-next"></use></svg>
    </a>
</div>
