<?php
    $product_name       = isset($product_name) ? $product_name : 'Product Name';
    $product_tagline    = isset($product_tagline) ? $product_tagline : 'Product Tagline';
    $product_story      = isset($product_story) ? $product_story : 'Product Story';
    $product_action     = isset($product_action) ? $product_action : 'Product Action';
?>
<div class="carousel-content">
    <h2 class="product-heading  text-white  h4"><?=$product_name?></h2>
    <h3 class="product-subheading  text-white  text-uppercase"><?=$product_tagline?></h3>
    <div class="product-brief  text-white"><?=$product_story?></div>
    <div class="product-action  mt-5">
        <a href="#" class="btn  btn-pill  btn-primary">
            <span><?=$product_action?></span>
            <svg class="icon-chevron-next" width="24" height="24" fill="currentColor"><use xlink:href="#icon-chevron-next"></use></svg>
        </a>
    </div>
</div>
