<div id="banners" class="carousel  carousel-banners  slide  text-white" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <?php include 'banners-content.php'; ?>
        </div>
        <div class="carousel-item">
            <?php include 'banners-content.php'; ?>
        </div>
    </div>

    <?php include 'banners-control.php'; ?>
</div>
