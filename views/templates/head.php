<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AUDRA</title>
    <link href="//fonts.googleapis.com/css?family=Rubik:100,300,400,500,700&display=swap" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Viga&display=swap" rel="stylesheet">
    <link href="assets/css/theme.css" rel="stylesheet" type="text/css">
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
    <?php include 'icons.php'; ?>
