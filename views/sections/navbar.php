<nav class="navbar  navbar-expand-lg">
    <div class="container">
        <a class="navbar-brand" href="index.php">
            <img src="assets/images/logo.svg" height="32" width="157" alt="Audra">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon  d-flex  align-items-center  justify-content-center  text-primary">
                <svg class="icon-burger" width="28" height="28" fill="currentColor"><use xlink:href="#icon-burger"></use></svg>
            </span>
        </button>

        <div class="collapse navbar-collapse" id="navbar-menu">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item  active">
                    <a class="nav-link" href="homeshield.php">HomeShield</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="bizsecure.php">BiZSecure</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="audra-cloud.php">Audra Cloud</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="isp.php">ISP</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="partner.php">Partner</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="buy-now.php">Buy Now</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
