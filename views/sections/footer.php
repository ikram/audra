
<div class="footer">
    <div class="container  container-slim  pt-5">
        <div class="footer-quote  d-flex  align-items-center  justify-content-between">
            <div class="footer-quote-text">
                We work with great people and partners in their journey to
                digital wellness. We’d like to be part of yours. Let’s talk!
            </div>
            <div class="footer-quote-button">
                <a href="#" class="btn  btn-lg  btn-pill">Contact Us</a>
            </div>
        </div>
    </div>
    <div class="container  pb-5">
        <hr class="divider  my-5  d-none  d-sm-block">

        <div class="row">
            <div class="col-md-4">
                <a data-toggle="collapse" href=".footer-sitemap">
                    <span>Sitemap</span>
                    <svg class="icon-plus  ml-auto" width="20" height="20" fill="currentColor"><use xlink:href="#icon-plus"></use></svg>
                </a>
                <div class="footer-sitemap  collapse">
                    <div class="row">
                        <div class="col-md-6">
                            <ul class="footer-menu  list-unstyled">
                                <li><a href="homeshield.php">HomeShield</a></li>
                                <li><a href="bizsecure.php">BiZSecure</a></li>
                                <li><a href="audra-cloud.php">Audra Cloud</a></li>
                                <li><a href="isp.php">ISP</a></li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul class="footer-menu  list-unstyled">
                                <li><a href="partner.php">Partners</a></li>
                                <li>
                                    <a href="about-us.php">About Us</a>
                                    <ul class="footer-submenu  list-unstyled  small">
                                        <li><a href="media.php">Media</a></li>
                                        <li><a href="contact-us.php">Contact</a></li>
                                    </ul>
                                </li>
                                <li><a href="buy-now.php">Buy Now</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <a data-toggle="collapse" href=".footer-office">
                    <span>Locate Us</span>
                    <svg class="icon-plus  ml-auto" width="20" height="20" fill="currentColor"><use xlink:href="#icon-plus"></use></svg>
                </a>
                <div class="footer-office  collapse">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="office-name  font-weight-bold  text-primary  text-uppercase">DOTLINES TRADING PTE LTD</div>
                            <div class="office-address  small">
                                #09-08A<br>
                                Golden Wall Center<br>
                                89 Short Street<br>
                                Singapore 188216
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="office-name  font-weight-bold  text-primary  text-uppercase">DOTLINES INC</div>
                            <div class="office-address  small">
                                Suite B-2<br>
                                2035 Sunset Lake Road<br>
                                Newark, New Castle<br>
                                Delaware, U.S.A 19702
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="office-name  font-weight-bold  text-primary  text-uppercase">DOTLINES SDN BHD</div>
                            <div class="office-address  small">
                                Unit 26-3, Tower A<br>
                                The Vertical Business Suites<br>
                                Bangsar South City, #8,<br>
                                Jalan Kerinchi Kuala Lumpur,<br>
                                Malaysia 5920
                            </div>
                        </div>
                    </div>
                </div>

                <ul class="footer-social  list-unstyled">
                    <li>
                        <a target="_blank" href="https://www.facebook.com/audra.io/">
                            <svg class="icon-facebook" width="20" height="20" fill="currentColor"><use xlink:href="#icon-facebook"></use></svg>
                            <span>Facebook</span>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="https://twitter.com/audra_io">
                            <svg class="icon-twitter" width="20" height="20" fill="currentColor"><use xlink:href="#icon-twitter"></use></svg>
                            <span>Twitter</span>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="https://www.instagram.com/audra_io/">
                            <svg class="icon-instagram" width="20" height="20" fill="currentColor"><use xlink:href="#icon-instagram"></use></svg>
                            <span>Instagram</span>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="https://www.linkedin.com/company/dotlines/">
                            <svg class="icon-linkedin" width="20" height="20" fill="currentColor"><use xlink:href="#icon-linkedin"></use></svg>
                            <span>Linkedin</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="footer-copyright  small  text-center  text-muted">
            &copy; Copyright 2019. All Rights Reserved, Dotlines Sdn. Bhd.
        </div>
    </div>
</div>
