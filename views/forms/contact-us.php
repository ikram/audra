<form class="form-contact-us  form-custom">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="form-label">Name</label>
                <div class="form-input">
                    <input type="text" class="form-control" />
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="form-label">Email</label>
                <div class="form-input">
                    <input type="text" class="form-control" />
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label class="form-label">Subject</label>
                <div class="form-input">
                    <input type="text" class="form-control" />
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label class="form-label">Message</label>
                <div class="form-input">
                    <textarea class="form-control  mb-4" rows="10"></textarea>

                    <button class="btn  btn-primary">
                        Submit
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
